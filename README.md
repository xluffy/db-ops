# Summary

This repository is the place for tooling around database operations to enable
automation of common tasks.

## Goals

* Improve reliability and reduce toil of database operations through automation
* Run operations through CI pipelines if possible
* Integrate with Chat-Ops

# Setup

## Console Server

### Clone db-ops repo

```bash
cd ~
mkdir -p workspace
cd workspace
git clone git@ops.gitlab.net:gitlab-com/gl-infra/db-ops.git
cd db-ops
ln -s bin/pre-commit .git/hooks/pre-commit
```

### Install Python 3.7

#### GNU Linux only

Install pipenv and Python 3.7 in your home directory.

```bash
export PATH="$HOME/.local/bin:$PATH"  # Add to .bash_profile
mkdir -p tmp
pushd tmp
curl --silent --show-error --remote-name --location https://www.python.org/ftp/python/3.7.9/Python-3.7.9.tgz
tar -zxf Python-3.7.9.tgz
pushd Python-3.7.9/
./configure --prefix=$HOME/.local/var/opt/python-3.7.9
make && make install
$HOME/.local/var/opt/python-3.7.9/bin/python3 -m pip install --upgrade pip
$HOME/.local/var/opt/python-3.7.9/bin/python3 -m pip install --user pipenv
pipenv --python $HOME/.local/var/opt/python-3.7.9/bin/python3 shell
popd
popd
```

#### Darwin only

Install Python 3.7 and pipenv in a virtual environment.

```bash
mkdir -p tmp
pushd tmp
curl --silent --show-error --remote-name --location https://www.python.org/ftp/python/3.7.9/python-3.7.9-macosx10.9.pkg
sudo installer -pkg python-3.7.9-macosx10.9.pkg -target '/Volumes/Macintosh HD' -verbose -dumplog 2>&1 | tee installer_$(date -u +'%Y-%m-%d_%H%M%S').log
popd
python3.7 -m pip install --upgrade pip
python3.7 -m venv db-ops-virtual-environment
source db-ops-virtual-environment/bin/activate
# To deactivate the virtual environment context session:
# deactivate
python3.7 -m pip install pipenv
```

### Sync pipenv

```bash
PIPENV_VERBOSITY=-1 pipenv sync
```

# Playbooks

Always run inside a `screen` or `tmux` session on a console node for consistency
(setup see above)!

## Restart Patroni Replicas

Restarts postgres on all replicas one-by-one, taking care of traffic draining.

Important vars (and their defaults):

```yaml
# how often and with which delay to check for connections to drain before terminating them
drain_check_retries: 60
drain_check_interval_s: 4

# If run_edit_config is true, we will set max_connections centrally via DCS
run_edit_config: false
max_connections: 500
```

### Execution

Make sure the inventory files are reflecting reality!

```bash
pipenv --python $HOME/.local/var/opt/python-3.7.9/bin/python3 shell
pipenv sync
tmux
```

Usage:

```bash
# ansible-playbook --inventory-file=ansible/inventory/gstg.yml [--step] [--check] [--extra-vars="run_edit_config=true"] ansible/playbooks/restart_postgres.yml
```

Example "dry-run" (check) invocation:

```bash
ANSIBLE_LOG_PATH=log/playbook_restart_postgres_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --inventory-file=ansible/inventory/gstg.yml --check ansible/playbooks/restart_postgres.yml
```

Example invocation:

```bash
ANSIBLE_LOG_PATH=log/playbook_restart_postgres_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --inventory-file=ansible/inventory/gstg.yml ansible/playbooks/restart_postgres.yml
```


## Switchover Primary

Switchover the Primary Patroni node. The play does some sanity checks and
executes checkpoints on primary and switchover candidate before executing a
switchover. If no `candidate` extra_var is given it will switch over to the
first replica node in the inventory.

### Execution

Make sure the inventory files are reflecting reality!

```bash
pipenv --python $HOME/.local/var/opt/python-3.7.9/bin/python3 shell
pipenv sync
tmux
ansible-inventory --inventory-file=ansible/inventory/gstg.yml --list | jq ".secondary.hosts"
```

Usage:

```bash
ansible-playbook --inventory-file=ansible/inventory/gstg.yml [--step] [--check] [--extra-vars="candidate=patroni-02-db-gstg.c.gitlab-staging-1.internal"] ansible/playbooks/switchover_primary.yml
```


## Enable checksums on replicas

Enable checksums on a replica database by invoking `pg_checksums --enable`.

### Execution

Make sure the inventory files are reflecting reality!

```bash
pipenv --python $HOME/.local/var/opt/python-3.7.9/bin/python3 shell
pipenv sync
tmux
ansible-inventory --inventory-file=ansible/inventory/gstg.yml --list | jq ".secondary.hosts"
```

Usage:

```bash
# ansible-playbook --inventory-file=ansible/inventory/gstg.yml [--step] [--check] ansible/playbooks/pg_checksums_enable.yml
```

Confirm the targets:

```bash
ansible-inventory --inventory-file=ansible/inventory/gstg.yml --list | jq ".secondary.hosts"
```

Check syntax of playbook:

```bash
ansible-playbook --inventory-file=ansible/inventory/gstg.yml --list-tasks ansible/playbooks/pg_checksums_enable.yml
```

Example "dry-run" (check) invocation:

```bash
ANSIBLE_LOG_PATH=log/playbook_pg_checksums_enable_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --inventory-file=ansible/inventory/gstg.yml --limit=patroni-07-db-gstg.c.gitlab-staging-1.internal --check ansible/playbooks/pg_checksums_enable.yml
```

Example invocation, stepping through the playbook carefully, requesting user confirmation before each task:

```bash
ANSIBLE_LOG_PATH=log/playbook_pg_checksums_enable_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --step --inventory-file=ansible/inventory/gstg.yml --limit=patroni-07-db-gstg.c.gitlab-staging-1.internal ansible/playbooks/pg_checksums_enable.yml
```

To execute or resume playbook execution from a specified task:

```bash
ANSIBLE_LOG_PATH=log/playbook_pg_checksums_enable_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --start-at-task="Verify that the Patroni Service is inactive" --step --inventory-file=ansible/inventory/gstg.yml --limit=patroni-07-db-gstg.c.gitlab-staging-1.internal ansible/playbooks/pg_checksums_enable.yml
```


## Disable checksums on replicas

Disable checksums on a replica database by invoking `pg_checksums --disable`.

### Execution

Make sure the inventory files are reflecting reality!

```bash
pipenv --python $HOME/.local/var/opt/python-3.7.9/bin/python3 shell
pipenv sync
tmux
ansible-inventory --inventory-file=ansible/inventory/gstg.yml --list | jq ".secondary.hosts"
```

Usage:

```bash
# ansible-playbook --inventory-file=ansible/inventory/gstg.yml [--step] [--check] ansible/playbooks/pg_checksums_disable.yml
```

Confirm the targets:

```bash
ansible-inventory --inventory-file=ansible/inventory/gstg.yml --list | jq ".secondary.hosts"
```

Check syntax of playbook:

```bash
ansible-playbook --inventory-file=ansible/inventory/gstg.yml --list-tasks ansible/playbooks/pg_checksums_disable.yml
```

Example "dry-run" (check) invocation:

```bash
ANSIBLE_LOG_PATH=log/playbook_pg_checksums_disable_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --inventory-file=ansible/inventory/gstg.yml --limit=patroni-07-db-gstg.c.gitlab-staging-1.internal --check ansible/playbooks/pg_checksums_disable.yml
```

Example invocation:

```bash
ANSIBLE_LOG_PATH=log/playbook_pg_checksums_disable_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --inventory-file=ansible/inventory/gstg.yml --limit=patroni-07-db-gstg.c.gitlab-staging-1.internal ansible/playbooks/pg_checksums_disable.yml
```

To execute or resume playbook execution from a specified task:

```bash
ANSIBLE_LOG_PATH=log/playbook_pg_checksums_disable_$(date -u +'%Y-%m-%d_%H%M%S').log ansible-playbook --start-at-task="Verify that the Patroni Service is inactive" --step --inventory-file=ansible/inventory/gstg.yml --limit=patroni-07-db-gstg.c.gitlab-staging-1.internal ansible/playbooks/pg_checksums_disable.yml
```

